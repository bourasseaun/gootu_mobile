import React, {useContext} from 'react';
import {Center} from '../components/Center';
import {AuthNavProps} from '../navigation/AuthParamList';
import {AuthContext} from '../navigation/AuthProvider';
import {
  Text,
  TextInput,
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
} from 'react-native';

export function Login({navigation}: AuthNavProps<'Login'>) {
  const {login} = useContext(AuthContext);
  //Style
  const styles = StyleSheet.create({
    colorWhite: {color: '#FFFFFF'},
    connexionTitle: {textTransform: 'uppercase', color: '#FFF', fontSize: 50},
    ViewGlobal: {flex: 1, paddingTop: 80},
    logo: {width: 200, height: 100, resizeMode: 'contain', marginBottom: 80},
    menuButtonConnect: {backgroundColor: '#349d5a'},
    menuOnglets: {
      backgroundColor: 'rgba(255,255,255,0.3)',
      width: '100%',
      flexDirection: 'row',
      paddingTop: 10,
      paddingBottom: 10,
      justifyContent: 'center',
    },
    buttonConnectModel: {
      paddingTop: 10,
      paddingBottom: 10,
      paddingLeft: 20,
      paddingRight: 20,
      borderRadius: 50,
    },
    buttonConnect: {
      backgroundColor: '#ED751A',
      paddingRight: 30,
      paddingLeft: 30,
    },
    menuButtonAccount: {
      paddingTop: 10,
      paddingBottom: 10,
      paddingRight: 20,
      paddingLeft: 20,
      color: '#ED751A',
    },
    inputsConnect: {marginTop: 20, marginBottom: 40, width: '80%'},
    unInputConnect: {width: '100%'},
  });
  return (
    <ImageBackground
      source={require('../assets/background.jpg')}
      style={styles.ViewGlobal}>
      <Center>
        <Image
          style={styles.logo}
          source={require('../assets/logoGootu.png')}
        />
        <View style={styles.menuOnglets}>
          <TouchableOpacity
            onPress={() => {
              null;
            }}>
            <Text
              style={[
                styles.menuButtonConnect,
                styles.buttonConnectModel,
                styles.colorWhite,
              ]}>
              Se connecter
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Register');
            }}>
            <Text style={styles.menuButtonAccount}>Créer mon compte</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.inputsConnect}>
          <TextInput
            underlineColorAndroid={'#349d5a'}
            style={styles.unInputConnect}
            placeholder="Email"
          />
          <TextInput
            underlineColorAndroid={'#349d5a'}
            style={styles.unInputConnect}
            placeholder="Mot de passe"
            placeholderTextColor="#717572"
          />
        </View>
        <TouchableOpacity
          onPress={() => {
            login();
          }}>
          <Text
            style={[
              styles.buttonConnectModel,
              styles.buttonConnect,
              styles.colorWhite,
            ]}>
            SE CONNECTER
          </Text>
        </TouchableOpacity>
      </Center>
    </ImageBackground>
  );
}
