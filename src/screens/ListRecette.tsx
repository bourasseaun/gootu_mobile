import React, {useState, useEffect} from 'react';
import {View, Text, Image, StyleSheet, AsyncStorage} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import * as axios from 'axios';
import {
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import {HomeStacknavProps} from '../navigation/HomeParamList';

export function ListRecette({navigation}: HomeStacknavProps<'Home'>) {
  const [AllRecettes, setAllRecettes] = useState({});

  useEffect(() => {
    GetALLRecette();
  }, []);

  function GetALLRecette() {
    NetInfo.fetch().then(state => {
      console.log('Connection type', state.type);
      console.log('Is connected?', state.isConnected);
      if (state.isConnected) {
        axios
          .get('http://gootu.juliendrieu.fr/gootu_api/V3/api/API.php')
          .then(function(response) {
            console.log('Reponse API', response.data);
            var dataAPI = response.data;
            setAllRecettes(dataAPI);
            return dataAPI[0];
          });
      }
    });
  }

  //   removeAll = function(ary, elem) {
  //     return ary.filter(function(e) { return e != elem });
  // }
  function ToggleFav(idFav) {
    AsyncStorage.getItem('RecetteFav', (err, result) => {
      const NewFav = idFav;
      console.log(idFav);
      console.log(err);
      if (result !== null) {
        console.log('Data Found', result);
        var TamponFavList = JSON.parse(result);
        console.log(TamponFavList);
        if (TamponFavList.indexOf(idFav) > -1) {
          var NewFavList = TamponFavList.filter(function(e) {
            return e !== idFav;
          });
        } else {
          var NewFavList = TamponFavList.push(idFav);
        }
        console.log(TamponFavList);
        AsyncStorage.setItem('RecetteFav', JSON.stringify(NewFavList));
      } else {
        console.log('Data Not Found');
        var arrayFav = [NewFav];
        AsyncStorage.setItem('RecetteFav', JSON.stringify(arrayFav));
      }
    });
  }
  function Vignette({TitreRecette, URLImage, Createur, idRecette}) {
    return (
      <TouchableOpacity style={styles.vignette} onPress={() => null}>
        <TouchableWithoutFeedback>
          <TouchableOpacity onPress={() => ToggleFav(idRecette)}>
            <Text>Favori</Text>
          </TouchableOpacity>
        </TouchableWithoutFeedback>
        <Text>{TitreRecette}</Text>
        <Image style={styles.tinyLogo} source={{uri: URLImage}} />
        <Text>{Createur}</Text>
      </TouchableOpacity>
    );
  }

  const styles = StyleSheet.create({
    tinyLogo: {
      width: 50,
      height: 50,
    },
    global: {
      display: 'flex',
      flexWrap: 'wrap',
      flexDirection: 'row',
    },
    vignette: {
      height: 150,
      width: 150,
    },
  });
  return (
    <View style={styles.global}>
      <FlatList
        data={AllRecettes}
        renderItem={({item}) => (
          <Vignette
            TitreRecette={item.nom}
            URLImage={item.pic}
            Createur={item.creator}
            idRecette={item.id}
          />
        )}
      />
    </View>
  );
}
