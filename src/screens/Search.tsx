/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {Center} from '../components/Center';
import {Text, TouchableOpacity, View, StyleSheet, Image} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import * as axios from 'axios';

export function Search() {
  const [RecetteActuelle, setRecetteActuelle] = useState({
    id: -1,
    idtexte: 'idtexte',
    nom: 'nom',
    pic: 'pic',
    creator: '',
    personne: {nb_personne: 0, libelle: 'libelle'},
    etapes: ['etapes1', 'etapes2'],
    ingredients: [
      {libelle: 'ingredients1', quantite: 1, mesure: 'g'},
      {libelle: 'ingredients2', quantite: 2, mesure: 'g'},
    ],
  });
  const [NbPersonne, setNbPersonne] = useState(0);
  const [NumEtape, setNumEtape] = useState(0);
  const [RecetteVue, setRecetteVue] = useState(Array);

  useEffect(() => {
    GetRecette();
  }, []);

  function GetRecette() {
    NetInfo.fetch().then(state => {
      console.log('Connection type', state.type);
      console.log('Is connected?', state.isConnected);
      if (state.isConnected) {
        axios
          .get('http://gootu.juliendrieu.fr/gootu_api/V3/api/API.php')
          .then(function(response) {
            console.log('typeof NbPersonne', typeof NbPersonne);
            console.log('Reponse API', response.data);
            var dataAPI = response.data;
            //setRecetteActuelle(dataAPI[0]);
            setNbPersonne(Number(dataAPI[0].personne.nb_personne));
            setRecetteActuelle({
              id: Number(dataAPI[0].id),
              idtexte: dataAPI[0].id,
              nom: dataAPI[0].nom,
              pic: dataAPI[0].pic,
              creator: dataAPI[0].creator,
              personne: {
                nb_personne: Number(dataAPI[0].personne.nb_personne),
                libelle: dataAPI[0].personne.libelle,
              },
              etapes: dataAPI[0].etapes,
              ingredients: dataAPI[0].ingredients,
            });
            if (RecetteVue.indexOf(RecetteActuelle.id) <= -1) {
              setRecetteVue([...RecetteVue, RecetteActuelle.id]);
            }
            console.log(dataAPI[0].id);
            console.log(dataAPI[0].ingredients);
            console.log(RecetteVue[1]);
            return dataAPI[0];
          });
      }
    });
  }
  function backRecette() {
    console.log(RecetteActuelle.id);
  }
  function nextRecette() {
    console.log(RecetteActuelle.id);
  }
  function MoinsPersonne() {
    setNbPersonne(NbPersonne - 1);
    console.log(typeof {NbPersonne}, {NbPersonne}.NbPersonne);
  }
  function PlusPersonne() {
    setNbPersonne(NbPersonne + 1);
  }
  function BackEtape() {
    setNumEtape(NumEtape - 1);
    console.log(RecetteActuelle.id);
  }
  function NextEtape() {
    setNumEtape(NumEtape + 1);
    console.log(RecetteActuelle.id);
  }

  //Styles
  const styles = StyleSheet.create({
    MenuRecette: {
      flex: 1,
      width: '100%',
      backgroundColor: 'pink',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    Button: {
      alignItems: 'center',
      justifyContent: 'center',
      padding: 10,
      borderRadius: 10,
      maxHeight: 30,
    },
    MenuNbPersonne: {
      flex: 1,
      width: '100%',
      backgroundColor: 'lightgrey',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    MenuEtape: {
      flex: 1,
      width: '100%',
      backgroundColor: 'grey',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
  });
  return (
    <Center>
      <Text>Search</Text>
      <TouchableOpacity activeOpacity={0.5} onPress={() => GetRecette()}>
        <Text>Importer des articles</Text>
      </TouchableOpacity>
      <View style={styles.MenuRecette}>
        <TouchableOpacity
          style={[
            styles.Button,
            RecetteVue[1] === RecetteActuelle.id
              ? {backgroundColor: 'lightblue'}
              : {backgroundColor: 'blue'},
          ]}
          activeOpacity={0.5}
          onPress={() => backRecette()}>
          <Text>Précedent</Text>
        </TouchableOpacity>
        <Text>{RecetteActuelle.id}</Text>
        <TouchableOpacity
          style={[styles.Button, {backgroundColor: 'blue'}]}
          activeOpacity={0.5}
          onPress={() => nextRecette()}>
          <Text>Suivant</Text>
        </TouchableOpacity>
      </View>
      <Image
        source={{uri: 'https://reactnative.dev/img/tiny_logo.png'}}
        style={{width: 200, height: 200}}
      />
      <View style={styles.MenuNbPersonne}>
        <TouchableOpacity
          style={[
            styles.Button,
            {NbPersonne}.NbPersonne <= 1
              ? {backgroundColor: 'lightyellow'}
              : {backgroundColor: 'yellow'},
          ]}
          activeOpacity={0.5}
          disabled={{NbPersonne}.NbPersonne <= 1}
          onPress={() => MoinsPersonne()}>
          <Text>Précedent</Text>
        </TouchableOpacity>
        <Text>
          {NbPersonne} {RecetteActuelle.personne.libelle}
          {NbPersonne === RecetteActuelle.personne.nb_personne ? '*' : ''}
        </Text>
        <TouchableOpacity
          style={[styles.Button, {backgroundColor: 'yellow'}]}
          activeOpacity={0.5}
          onPress={() => PlusPersonne()}>
          <Text>Suivant</Text>
        </TouchableOpacity>
      </View>
      <View>
        {RecetteActuelle.ingredients.map((prop, key) => {
          return (
            <Text key={key}>
              {prop.libelle}{' '}
              {(prop.quantite * NbPersonne) /
                RecetteActuelle.personne.nb_personne}
              {prop.mesure}
            </Text>
          );
        })}
      </View>
      <View style={styles.MenuEtape}>
        <TouchableOpacity
          style={[
            styles.Button,
            {NumEtape}.NumEtape <= 0
              ? {backgroundColor: 'lightgreen'}
              : {backgroundColor: 'green'},
          ]}
          disabled={{NumEtape}.NumEtape <= 0}
          activeOpacity={0.5}
          onPress={() => BackEtape()}>
          <Text>Précedent</Text>
        </TouchableOpacity>
        <Text>{RecetteActuelle.etapes[NumEtape]}</Text>
        <TouchableOpacity
          style={[styles.Button, {backgroundColor: 'green'}]}
          activeOpacity={0.5}
          onPress={() => NextEtape()}>
          <Text>Suivant</Text>
        </TouchableOpacity>
      </View>
    </Center>
  );
}
