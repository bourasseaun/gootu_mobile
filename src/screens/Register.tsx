import React from 'react';
import {Center} from '../components/Center';
import {AuthNavProps} from '../navigation/AuthParamList';
import {
  Text,
  TextInput,
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
} from 'react-native';

function register() {
  return null;
}
export function Register({navigation}: AuthNavProps<'Register'>) {
  const styles = StyleSheet.create({
    colorWhite: {color: '#FFFFFF'},
    connexionTitle: {textTransform: 'uppercase', color: '#FFF', fontSize: 50},
    ViewGlobal: {flex: 1, paddingTop: 80},
    logo: {width: 200, height: 100, resizeMode: 'contain', marginBottom: 80},
    menuButtonConnect: {backgroundColor: '#349d5a'},
    menuOnglets: {
      backgroundColor: 'rgba(255,255,255,0.3)',
      width: '100%',
      flexDirection: 'row',
      paddingTop: 10,
      paddingBottom: 10,
      justifyContent: 'center',
    },
    buttonConnectModel: {
      paddingTop: 10,
      paddingBottom: 10,
      paddingLeft: 20,
      paddingRight: 20,
      borderRadius: 50,
    },
    buttonConnect: {
      backgroundColor: '#ED751A',
      paddingRight: 30,
      paddingLeft: 30,
    },
    menuButtonAccount: {
      paddingTop: 10,
      paddingBottom: 10,
      paddingRight: 20,
      paddingLeft: 20,
      color: '#ED751A',
    },
    inputsConnect: {marginTop: 20, marginBottom: 40, width: '80%'},
    unInputConnect: {width: '100%'},
  });
  return (
    <ImageBackground
      source={require('../assets/background.jpg')}
      style={styles.ViewGlobal}>
      <Center>
        <Image
          style={styles.logo}
          source={require('../assets/logoGootu.png')}
        />
        <View style={styles.menuOnglets}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Login');
            }}>
            <Text style={styles.menuButtonAccount}>Se connecter</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              null;
            }}>
            <Text
              style={[
                styles.menuButtonConnect,
                styles.buttonConnectModel,
                styles.colorWhite,
              ]}>
              Créer mon compte
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.inputsConnect}>
          <TextInput
            underlineColorAndroid={'#349d5a'}
            style={styles.unInputConnect}
            placeholder="Email"
          />
          <TextInput
            underlineColorAndroid={'#349d5a'}
            style={styles.unInputConnect}
            placeholder="Mot de passe"
            placeholderTextColor="#717572"
          />
        </View>
        <TouchableOpacity
          onPress={() => {
            register();
          }}>
          <Text
            style={[
              styles.buttonConnectModel,
              styles.buttonConnect,
              styles.colorWhite,
            ]}>
            SE CONNECTER
          </Text>
        </TouchableOpacity>
      </Center>
    </ImageBackground>
  );
}
