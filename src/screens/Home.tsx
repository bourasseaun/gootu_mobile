import React, {useContext} from 'react';
import {Center} from '../components/Center';
import {AuthContext} from '../navigation/AuthProvider';
import {Button, Text, TouchableOpacity} from 'react-native';
import {HomeStacknavProps, HomeParamList} from '../navigation/HomeParamList';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator<HomeParamList>();

export function Home({navigation}: HomeStacknavProps<'Home'>) {
  const {logout} = useContext(AuthContext);
  return (
    <Center>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Home');
        }}>
        <Text>Home</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('ListRecette');
        }}>
        <Text>ListRecette</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          return null;
        }}>
        <Text>Undefined</Text>
      </TouchableOpacity>
      <Button title="logout" onPress={() => logout()} />
    </Center>
  );
}
