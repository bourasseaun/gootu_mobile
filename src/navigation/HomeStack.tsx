import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Home} from '../screens/Home';
import {ListRecette} from '../screens/ListRecette';

interface HomeStackProps {}

const Stack = createStackNavigator();

export const AuthStack: React.FC<HomeStackProps> = ({}) => {
  return (
    <Stack.Navigator
      screenOptions={{
        header: () => null,
      }}
      initialRouteName="Home">
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="ListRecette" component={ListRecette} />
    </Stack.Navigator>
  );
};
