import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {AppParamList} from './AppParamList';
import {Home} from '../screens/Home';
import {Search} from '../screens/Search';
import {ListRecette} from '../screens/ListRecette';

interface AppTabsProps {}

const Drawer = createDrawerNavigator<AppParamList>();

export const AppTabs: React.FC<AppTabsProps> = ({}) => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Home" component={Home} />
      <Drawer.Screen name="ListRecette" component={ListRecette} />
      <Drawer.Screen name="Search" component={Search} />
    </Drawer.Navigator>
  );
};
